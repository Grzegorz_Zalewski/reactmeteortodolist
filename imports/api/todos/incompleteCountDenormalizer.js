import { _ } from 'meteor/underscore';
import { check } from 'meteor/check';

import { Todos } from './todos.js';
import { Lists } from '../lists/lists.js';

const incompleteCountDenormalizer = {
  _updateList(listId) {
    const incompleteCount = Todos.find({
      listId,
      checked: false,
    }).count();
    Lists.update(listId, { $set: { incompleteCount } });
  },
  afterInsertTodo(todo) {
    this._updateList(todo.listId);
  },
  afterUpdateTodo(selector, modifier) {
    check(modifier, { $set: Object });
    if (_.has(modifier.$set, 'checked')) {
      Todos.find(selector, { fields: { listId: 1 } }).forEach((todo) => {
        this._updateList(todo.listId);
      });
    }
  },
  afterRemoveTodos(todos) {
    todos.forEach(todo => this._updateList(todo.listId));
  },
};

export default incompleteCountDenormalizer;
