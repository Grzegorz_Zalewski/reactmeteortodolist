import { Meteor } from 'meteor/meteor';
import { Lists } from '../../api/lists/lists.js';
import { Todos } from '../../api/todos/todos.js';

// if the database is empty on server start, create some sample data.
Meteor.startup(() => {
  if (Lists.find().count() === 0) {
    const data = [
      {
        name: 'Lista Zakupów',
        items: [
          'cebula',
          'ziemniaki',
          'pomidory',
          'proszek do prania',
          'jedzenie dla kotów',
          'chleb',
        ],
      },
      {
        name: 'porządki wiosenne',
        items: [
          'naprawić kran',
          'odmalować pokój',
          'posprzątać garaż',
          'zrobić przęgląd samochodu',
        ],
      },
      {
        name: 'Technologie do opanowania',
        items: [
          'meteot.js',
          'react.js',
          'mongo db',
        ],
      },
    ];

    let timestamp = (new Date()).getTime();

    data.forEach((list) => {
      const listId = Lists.insert({
        name: list.name,
        incompleteCount: list.items.length,
      });

      list.items.forEach((text) => {
        Todos.insert({
          listId,
          text,
          createdAt: new Date(timestamp),
        });

        timestamp += 1; // ensure unique timestamp.
      });
    });
  }
});
