/* global alert */


export const displayError = (error) => {
  if (error) {
    alert(error); // eslint-disable-line no-alert
  }
};
