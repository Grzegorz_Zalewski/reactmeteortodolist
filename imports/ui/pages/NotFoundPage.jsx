import React from 'react';
import BaseComponent from '../components/BaseComponent.jsx';
import MobileMenu from '../components/MobileMenu.jsx';
import Message from '../components/Message.jsx';

class NotFoundPage extends BaseComponent {
  render() {
    return (
      <div className="page not-found">
        <nav>
          <MobileMenu />
        </nav>
        <div className="content-scrollable page jumbotron">
          <Message title="Page not found" />
        </div>
      </div>
    );
  }
}

export default NotFoundPage;
