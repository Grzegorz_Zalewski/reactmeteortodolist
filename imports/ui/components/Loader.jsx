import React from 'react';

const Loader = ({ text }) => (
  <div className="ui active dimmer">
    {text ? <div className="ui text loader">{text}</div> : null}
    <p />
  </div>
);

Loader.propTypes = {
  text: React.PropTypes.string,
};

export default Loader;
