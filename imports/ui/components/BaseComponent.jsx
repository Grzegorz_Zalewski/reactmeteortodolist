import { Component } from 'react';


class BaseComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
}

export default BaseComponent;
