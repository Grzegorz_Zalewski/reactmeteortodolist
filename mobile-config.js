/* globals App */
/* eslint-disable quote-props */

App.info({
  name: 'ToDoList ',
  description: 'ToDoList created in meteor with react specially for brainhub.eu',
  author: 'Grzegorz Zalewski',
  email: 'g.zalewski@prokonto.pl',
  version: '0.0.1',
});

App.icons({
  // iOS
  'iphone': 'public/icons/ios/icon.png',
  'iphone_2x': 'public/icons/ios/icon@2x.png',
  'iphone_3x': 'public/icons/ios/icon@3x.png',
  'ipad': 'public/icons/ios/icon-76.png',
  'ipad_2x': 'public/icons/ios/icon-76@2x.png',
  'ios_settings': 'public/icons/ios/icon-29.png',
  'ios_settings_2x': 'public/icons/ios/icon-29@2x.png',
  'ios_settings_3x': 'public/icons/ios/icon-29@3x.png',
  'ios_spotlight': 'public/icons/ios/icon-40.png',
  'ios_spotlight_2x': 'public/icons/ios/icon-40@2x.png',

  // Android
  'android_ldpi': 'public/icons/android/res/drawable-ldpi/ic_launcher.png',
  'android_mdpi': 'public/icons/android/res/drawable-mdpi/ic_launcher.png',
  'android_hdpi': 'public/icons/android/res/drawable-hdpi/ic_launcher.png',
  'android_xhdpi': 'public/icons/android/res/drawable-xhdpi/ic_launcher.png',
  'android_xxhdpi': 'public/icons/android/res/drawable-xxhdpi/ic_launcher.png',
  'android_xxxhdpi': 'public/icons/android/res/drawable-xxxhdpi/ic_launcher.png',
});

App.launchScreens({
  // iOS
  'iphone': 'resources/splash/ios/screen-iphone-landscape.png',
  'iphone_2x': 'resources/splash/ios/screen-iphone-landscape-2x.png',
  'iphone5': 'resources/splash/ios/screen-iphone-landscape-568h-2x.png',
  'iphone6': 'resources/splash/ios/screen-iphone-portrait-667h.png',
  'iphone6p_portrait': 'resources/splash/ios/screen-iphone-portrait-736h.png',
  'iphone6p_landscape': 'resources/splash/ios/screen-iphone-landscape-736h.png',
  'ipad_portrait': 'resources/splash/ios/screen-ipad-portrait.png',
  'ipad_portrait_2x': 'resources/splash/ios/screen-ipad-portrait-2x.png',
  'ipad_landscape': 'resources/splash/ios/screen-ipad-landscape.png',
  'ipad_landscape_2x': 'resources/splash/ios/screen-ipad-landscape-2x.png',

  // Android
  'android_ldpi_portrait': 'resources/splash/android/screen-ldpi-portrait.png',
  'android_ldpi_landscape': 'resources/splash/android/screen-ldpi-landscape.png',
  'android_mdpi_portrait': 'resources/splash/android/screen-mdpi-portrait.png',
  'android_mdpi_landscape': 'resources/splash/android/screen-mdpi-landscape.png',
  'android_hdpi_portrait': 'resources/splash/android/screen-hdpi-landscape.png',
  'android_hdpi_landscape': 'resources/splash/android/screen-hdpi-portrait.png',
  'android_xhdpi_portrait': 'resources/splash/android/screen-mdpi-portrait.png',
  'android_xhdpi_landscape': 'resources/splash/android/screen-mdpi-landscape.png',
});

App.setPreference('StatusBarOverlaysWebView', 'false');
App.setPreference('StatusBarBackgroundColor', '#ffffff');
